<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>Spring Eessential index page</title>
</head>
<body>
<h1>spring essential</h1>
<span>springframework를 사용하며 자주 사용하는 라이브러리/모듈를 모아두었습니다.</span><br/>
<span>소스는 <a href="https://bitbucket.org/kcheon/springessential" target="_blank">이곳</a>에서 다운로드 가능합니다.</span>
<ul>
	<li>웹 요청 Mapping 전략
		<ol>
			<li>DefaultAnnotationHandlerMapping<br/>
				Annotation 으로 정의한 Handler Entry 에서 웹요청과 맞는 Handler를 매핑</li>
			<li>BeanNameUrlHandlerMapping<br/>
				요청한 url과 같은 이름을 가진 bean에 웹요청 handling을 위임
			</li>
			<li>SimpleUrlHandlerMapping<br/>
				Controller에서 특별한 처리가 없는 경우 굳이 controller를 작성하고 매핑하는 번거로움을 피하기 위해 사용<br/>
				웹요청에 해당하는 viewResolver로 매핑 대개의 경우 InternalResourceViewResolver(JstlView)로 그 처리를 넘김<br/>
				<a href="essential/urlFilenameViewController.gujjy">UrlFilenameViewController Sample page</a>
			</li>
		</ol>
	</li>
	<br/>
	<li>웹 요청 Handler 전략
		<ol>
			<li>AnnotationMethodHandlerAdapter<br/>
				@RequestMapping Annotation 을 이용하여 웹요청처리 정보를 정의(주로 사용)
				<ul>
					<li>webBindingInitializer<br/>
						웹요청시 보내는 파라메터를 VO또는 DTO 등의 도메인 객체를 생성하며 그 값을 할당<br/>
						validator를 custom하게 DI
						<ul>
							<li>validator<br/>
								LocalValidatorFactoryBean: hibernate validator 기능사용
							</li>
						</ul>
					</li>
					<li>messageConverters : @ResponseBody로 반환하는 웹 요청결과 처리
						<ul>
							<li>ByteArrayHttpMessageConverter<br/>
								byte[]를 리턴하여 단순 stream 을 전송 img src에서 사용
							</li>
							<li>StringHttpMessageConverter<br/>
								String을 리턴하여 단순 test 를 전송
							</li>
							<li>MappingJacksonHttpMessageConverter
								String, byte[] 가 아닌 데이터를 Json으로 전송
							</li>
						</ul>
					</li>
				</ul>
			</li>
			<li>SimpleControllerHandlerAdapter<br/>
				Controller interface를 구현하는 Controller를 사용하여 웹요청을 처리(거의 사용하지 않음)
			</li>
		</ol>
	</li>
	<br/>
	<li>예외 처리 전략
		<ol>
			<li>AnnotationMethodHandlerExceptionResolver<br/>
				해당 클래스내에서 발생한 예외를 처리하기 위해 @ExceptionHandler Annotation을 사용하여 정의
				<ul>
					<li>messageConverters : @ResponseBody로 반환하는 웹 요청결과 처리<br/>
						AnnotationMethodHandlerAdapter의 messageConverters과 동일
					</li>
				</ul>
			</li>
			<li>SimpleMappingExceptionResolver<br/>
				공통적으로 사용하는 예외 처리하는 view를 구현하고 예외를 매핑하는 전략(defaultErrorView 미구현으로 미설정)
			</li>
		</ol>
	</li>
	<br/>
	<li>뷰 리졸버 전략
		<ol>
			<li>BeanNameViewResolver<br/>
				customView(ex. jsonView, excelView ...)를 정의하고 해당 View에 렌더를 위임하는 리졸버
			</li>
			<li>InternalResourceViewResolver<br/>
				웹요청처리 결과를 렌더할수 있는 자원에 랜더를 위임하는 리졸버 당 웹 프로그램은 JstlView를 사용
			</li>
		</ol>
	</li>
	<br/>
	<li>빈 등록 전략
		<ol>
			<li>component:scan
				<ul>
					<li>context : exclude controller<br/>
						웹 요청을 제외한 Bean과 기능 정의 @Service, @Repository<br/>
						transaction, aop, datasource ...
					</li>
					<li>servlet : only controller<br/>
						웹 요청 처리와 관련한 Bean 정의
					</li>
				</ul>
			</li>
			
		</ol>
	</li>
	<br/>
	<li> 스프링과 무관하지만 자주 쓰이는 것들
		<ul>
			<li>사이트메쉬<a href="sitemesh/content1.gujjy">sitemesh sample page</a>
		</ul>
	</li>
	<!-- 
	<li>
		<ol>
			<li></li>
			<li></li>
			<li></li>
			<li></li>
		</ol>
	</li> -->
</ul>
</body>
</html>