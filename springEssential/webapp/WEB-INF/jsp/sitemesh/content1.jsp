<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>sitemesh intro</title>
</head>
<body>
<div id='tabs' style="font-size: 14px;">
	<ul>
		<li><a href="#tab-1">introduction</a>
	</ul>
	<div id="tab-1">
		<h3>This pages are made with sitemesh 2</h3>
		<ul>
			<li><a href="http://wiki.sitemesh.org/dashboard.action" target="new">sitemesh wiki page</a>
			<li><a href="http://wiki.sitemesh.org/display/sitemesh/Home" target="new">sitemesh2 wiki page</a>
		</ul>
	</div>
</div>
<script>
$(function() {
	$( "#tabs" ).tabs();
});
</script>
</body>
</html>