<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>sitemesh configure</title>
</head>
<body>
<div id='tabs' style="font-size: 14px;">
	<ul>
		<li><a href="#tab-1">maven</a>
		<li><a href="#tab-2">web.xml</a>
		<li><a href="#tab-3">sitemesh.xml</a>
		<li><a href="#tab-4">decorators.xml</a>
	</ul>
	<div id="tab-1">
		<span>add dependency on maven pom.xml file</span><br/>
		<code>
			&lt;dependency&gt;<br/>
			&nbsp;&nbsp;&nbsp;&nbsp;&lt;groupId&gt;opensymphony&lt;/groupId&gt;<br/>
			&nbsp;&nbsp;&nbsp;&nbsp;&lt;artifactId&gt;sitemesh&lt;/artifactId&gt;<br/>
			&nbsp;&nbsp;&nbsp;&nbsp;&lt;version&gt;2.4.2&lt;/version&gt;<br/>
			&lt;/dependency&gt;<br/>
		</code>
	</div>
	
	<div id="tab-2">
		<span>add filter as first on web.xml</span><br/>
		<code>
			&lt;filter&gt;<br/>
			&nbsp;&nbsp;&nbsp;&nbsp;&lt;filter-name&gt;sitemesh&lt;/filter-name&gt;<br/>
			&nbsp;&nbsp;&nbsp;&nbsp;&lt;filter-class&gt;com.opensymphony.sitemesh.webapp.SiteMeshFilter&lt;/filter-class&gt;<br/>
			&lt;/filter&gt;<br/>
			<br/>
			&lt;filter-mapping&gt;<br/>
			&nbsp;&nbsp;&nbsp;&nbsp;&lt;filter-name&gt;sitemesh&lt;/filter-name&gt;<br/>
			&nbsp;&nbsp;&nbsp;&nbsp;&lt;url-pattern&gt;/*&lt;/url-pattern&gt;<br/>
			&lt;/filter-mapping&gt;<br/>
		</code>
	</div>
	
	<div id="tab-3">
		<span>same location with 'web.xml'</span><br/>
		<code>
			&lt;?xml version="1.0" encoding="UTF-8"?&gt;<br/>
			&lt;sitemesh&gt;<br/>
			&nbsp;&nbsp;&nbsp;&nbsp;&lt;property name="decorators-file" value="/WEB-INF/sitemesh/decorators.xml" /&gt;<br/>
			&nbsp;&nbsp;&nbsp;&nbsp;&lt;excludes file="&#36;{decorators-file}" /&gt;<br/>
			<br/>
			&nbsp;&nbsp;&nbsp;&nbsp;&lt;page-parsers&gt;<br/>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;parser content-type="text/html"<br/>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;class="com.opensymphony.module.sitemesh.parser.FastPageParser" /&gt;<br/>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;parser content-type="text/html; charset=utf-8"<br/>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;class="com.opensymphony.module.sitemesh.parser.FastPageParser" /&gt;<br/>
			&nbsp;&nbsp;&nbsp;&nbsp;&lt;/page-parsers&gt;<br/>
			<br/>
			&nbsp;&nbsp;&nbsp;&nbsp;&lt;decorator-mappers&gt;<br/>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;mapper<br/>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;class="com.opensymphony.module.sitemesh.mapper.ConfigDecoratorMapper"&gt;<br/>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;param name="config" value="&#36;{decorators-file}" /&gt;<br/>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/mapper&gt;<br/>
			&nbsp;&nbsp;&nbsp;&nbsp;&lt;/decorator-mappers&gt;<br/>
			<br/>
			&lt;/sitemesh&gt;<br/>
		</code>
	</div>
	<div id="tab-4">
		<span>'sitemesh.xml' file define decorators-file location</span><br/>
		<code>
			&lt;?xml version="1.0" encoding="UTF-8"?&gt;<br/>
			&lt;decorators&gt;<br/>
			&nbsp;&nbsp;&nbsp;&nbsp;&lt;excludes&gt;<br/>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;pattern&gt;**json**&lt;/pattern&gt;<br/>
			&nbsp;&nbsp;&nbsp;&nbsp;&lt;/excludes&gt;<br/>
			<br/>
			&nbsp;&nbsp;&nbsp;&nbsp;&lt;decorator name="default" page="/WEB-INF/sitemesh/default.jsp"&gt;<br/>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;pattern&gt;/sitemesh/*&lt;/pattern&gt;<br/>
			&nbsp;&nbsp;&nbsp;&nbsp;&lt;/decorator&gt;<br/>
			<br/>
			&lt;/decorators&gt;<br/>
		</code>
	</div>
</div>
<script>
$(function() {
	$('#tabs').tabs();
});
</script>
</body>
</html>