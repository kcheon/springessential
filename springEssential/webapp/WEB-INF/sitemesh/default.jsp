<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="deco" uri="http://www.opensymphony.com/sitemesh/decorator" %>
<%@ taglib prefix="page" uri="http://www.opensymphony.com/sitemesh/page" %>
<!DOCTYPE html>
<html lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<link type="text/css" rel="stylesheet" href="<c:url value="/css/jquery/redmond/jquery-ui-1.10.1.custom.min.css"/>"/>

<script type="text/javascript" src="<c:url value="/js/jquery/core/jquery-1.8.3.min.js"/>" ></script>
<script type="text/javascript" src="<c:url value="/js/jquery/core/jquery.cookie.js"/>" ></script>
<script type="text/javascript" src="<c:url value="/js/jquery/ui/i18n/jquery.ui.datepicker-ko.min.js"/>" ></script>
<script type="text/javascript" src="<c:url value="/js/jquery/ui/jquery-ui-1.10.1.custom.min.js"/>" ></script>
<script type="text/javascript" src="<c:url value="/js/jquery/grid/i18n/grid.locale-kr.js"/>" ></script>
<script type="text/javascript" src="<c:url value="/js/jquery/grid/jquery.jqGrid.src.4.4.1.js"/>" ></script>


<script>
$(function(){
	var path = location.pathname;
	$('#accordion h3').each(function(idx){
		if(path.indexOf(this.id)>0){
			$('#accordion').accordion({active:idx});		
		}else{
			$(this).click(function(){location.href=this.id+'.gujjy';});
		}
	});
	
});
</script>
<title><deco:title default="sitemesh default"></deco:title></title>
</head>
<body>
<div id="leftmenu" style="float: left;">
	<div id="accordion" style="width: 200px;font-size: 12px;">
		<h3 id="content1">intro</h3>
		<div>
			<p>sitemesh site links</p>
		</div>
		<h3 id="content2">config</h3>
		<div>
			<p>configure</p>
		</div>
		<h3 id="content3">comments</h3>
		<div>
			<p>some comment of sitemesh</p>
		</div>
	</div>
</div>
<div id="rightcontent" style="float:left;padding-left:15px;width:800px; ">	
	<deco:body/>
</div>
</body>
</html>