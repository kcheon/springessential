package kr.pe.gujjy.spring.essential.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.ResponseBody;

@RequestMapping(value={"essential"})
@Controller
public class EssentialController {

	@RequestMapping(value="stringResponseBody")
	@ResponseBody
	String stringResponseBody(){
		return "stringResponseBody"; //using string message converter 
	}
	
	
	@RequestMapping(value="string")
	String string(){
		return "string";	//view resolver(/WEB-INF/jsp/essential/string.jsp)
	}
}
