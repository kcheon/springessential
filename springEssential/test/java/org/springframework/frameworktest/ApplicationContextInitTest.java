package org.springframework.frameworktest;

import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;

import javax.sql.DataSource;

import net.sf.log4jdbc.DriverSpy;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;
import org.springframework.mock.jndi.SimpleNamingContextBuilder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath:/spring/context-*.xml")
public class ApplicationContextInitTest {
	

	@Autowired ApplicationContext context;
	
	@BeforeClass
	public static void setUpBefore() throws Exception{
		bindDataSourceWithNetSfSpyDriverAndSimpleNameContextBuilder();
        
	}
	
	static void bindDataSourceWithNetSfSpyDriverAndSimpleNameContextBuilder() throws Exception{
	
		DataSource ds = new SimpleDriverDataSource(
				new DriverSpy()
				, "jdbc:log4jdbc:hsqldb:res:hsqldb/springEssential"
				, "SA"
				, "");
		
//		DataSource ds2 = new SimpleDriverDataSource(
//				new DriverSpy()
//				, "jdbc:log4jdbc:hsqldb:res:hsqldb/springEssential"
//				, "iparrow"
//				, "emforhs02");
		
		SimpleNamingContextBuilder builder = SimpleNamingContextBuilder.emptyActivatedContextBuilder();
		builder.bind("java/www", ds);
//		builder.bind("java/news", ds2);

	}
	
	
	@Test
	public void appcontextDiTest() throws Exception {
		assertThat(context, notNullValue());
	}
}
