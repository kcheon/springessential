package org.springframework.frameworktest;

import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.stereotype.Controller;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.annotation.AnnotationMethodHandlerAdapter;

/**
 * 
 * @author KH
 * http://www.java-allandsundry.com/2012/08/contextconfiguration-defaults.html
 */
@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(locations={"classpath:/spring/webcontextTest-servlet.xml"})
//@ContextConfiguration(loader=AnnotationConfigContextLoader.class, classes={ExceptionHandleTestController.class})
@ContextConfiguration(loader=AnnotationConfigContextLoader.class, classes={TestConfiguration.class})
@DirtiesContext
public class ComponentScanInTestTest {
	Logger log = Logger.getLogger(this.getClass());
	
	@Autowired AnnotationMethodHandlerAdapter adapter;
	@Autowired ComponentScanTestController controller;
	
	MockHttpServletRequest req;
	MockHttpServletResponse resp;
	
	@Before
	public void setUp() throws Exception {
		req = new MockHttpServletRequest();
		resp = new MockHttpServletResponse();
	}

	@Test
	public void testDi() {
		assertThat(adapter, notNullValue());
		assertThat(controller, notNullValue());
	}
	
	@Test
	public void testNormal() throws Exception {
		req.setRequestURI("/normal");
		
		ModelAndView mav = adapter.handle(req, resp,controller);
		assertThat(mav.getModel().get("aa").toString(), is("1"));
		assertThat(mav.getViewName(), is("normal"));
	}
	
	

}

@Configuration
//@ComponentScan(basePackages="org.springframework.frameworktest",useDefaultFilters=false, includeFilters={@ComponentScan.Filter(type=FilterType.ANNOTATION, value=Controller.class)})
@ImportResource(value={"classpath:/spring/webcontextTest-servlet.xml"})
class TestConfiguration{
	
	@Bean
	public ComponentScanTestController componentScanTestController(){
		return new ComponentScanTestController();
	}
}

//@Configuration
//@ComponentScan(basePackages="org.springframework.frameworktest",useDefaultFilters=false, includeFilters={@ComponentScan.Filter(type=FilterType.ANNOTATION, value=Controller.class)})
//@ImportResource(value={"classpath:/spring/webcontextTest-servlet.xml"})
@Controller
class ComponentScanTestController{
	Logger log = Logger.getLogger(this.getClass());
	
	@RequestMapping("normal")
	public String normal(Model model){
		model.addAttribute("aa", "1");
		log.debug("called");
		return "normal";
	}
}