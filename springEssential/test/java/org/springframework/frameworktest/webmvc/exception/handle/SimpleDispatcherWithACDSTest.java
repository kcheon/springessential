package org.springframework.frameworktest.webmvc.exception.handle;

import java.io.IOException;

import java.util.HashSet;

import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;

import javax.inject.Provider;
import javax.servlet.ServletException;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.frameworktest.util.AnnotationConfigDispatcherServlet;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockServletConfig;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.DispatcherServlet;

/**
 * http://toby.epril.com/?p=972
 * @author KH
 *
 */
public class SimpleDispatcherWithACDSTest {
	MockHttpServletResponse response = new MockHttpServletResponse(); 
	
	@Test 
	public void requestScope() throws ServletException, IOException { 
	    //MockServletConfig ctx = new MockServletConfig(); 
	    DispatcherServlet ds = new AnnotationConfigDispatcherServlet(HelloController.class, HelloService.class, RequestBean.class, BeanCounter.class); 
	    ds.init(new MockServletConfig()); 
	    BeanCounter counter = ds.getWebApplicationContext().getBean(BeanCounter.class); 
	    
	    ds.service(new MockHttpServletRequest("GET", "/hello"), this.response); 
	    assertThat(counter.addCounter, is(2)); 
	    assertThat(counter.size(), is(1)); 
	    
	    ds.service(new MockHttpServletRequest("GET", "/hello"), this.response); 
	    assertThat(counter.addCounter, is(4)); 
	    assertThat(counter.size(), is(2)); 
	}
	
	@RequestMapping("/")
	static class HelloController { 
	    @Autowired HelloService helloService; 
	    @Autowired Provider<RequestBean> requestBeanProvider; 
	    @Autowired BeanCounter beanCounter; 
	    
	    @SuppressWarnings("unchecked")
		@RequestMapping("hello") 
	    public String hello() { 
	        beanCounter.addCounter++; 
	        beanCounter.add(requestBeanProvider.get()); 
	        helloService.hello(); 
	        return ""; 
	    } 
	}
	
	static class HelloService { 
	    @Autowired Provider<RequestBean> requestBeanProvider; 
	    @Autowired BeanCounter beanCounter; 
	    
	    @SuppressWarnings("unchecked")
		public void hello() { 
	        beanCounter.addCounter++; 
	        beanCounter.add(requestBeanProvider.get()); 
	    } 
	}
	
	@Scope("request") 
	static class RequestBean {} 
	
	@SuppressWarnings({ "rawtypes", "serial" })
	static class BeanCounter extends HashSet { int addCounter = 0; };
}
