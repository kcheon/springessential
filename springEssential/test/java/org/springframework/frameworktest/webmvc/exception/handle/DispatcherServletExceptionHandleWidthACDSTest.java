package org.springframework.frameworktest.webmvc.exception.handle;

import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;


import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.springframework.frameworktest.util.AnnotationConfigDispatcherServlet;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockServletConfig;
import org.springframework.mock.web.MockServletContext;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.DispatcherServlet;


public class DispatcherServletExceptionHandleWidthACDSTest {
	private Logger log = Logger.getLogger(this.getClass());
	private DispatcherServlet servlet;
	
	private MockHttpServletRequest request;
	private MockHttpServletResponse response;
	private MockServletConfig sConfig;
	private MockServletContext sContext;
	
	@Before
	public void setUp() throws Exception {
		log.debug("setup");
		
		request = new MockHttpServletRequest();
		response = new MockHttpServletResponse();
		sContext = new MockServletContext();
		sConfig = new MockServletConfig(sContext, "gujjy");
		
		servlet = new AnnotationConfigDispatcherServlet(ExceptionHandleTestController.class);
		servlet.init(sConfig);
		
	}

	@Test
	public void testDi() {
		assertThat(servlet, notNullValue());
	}
	
	
	
	@Test
	public void testNormal_HEAD() throws Exception {
		request.setMethod("HEAD");
		request.setRequestURI("/normal.gujjy");
		servlet.service(request, response);
		assertThat(request.getAttribute("aa").toString(), is("1"));
		assertThat(response.getForwardedUrl(), is("normal"));
		log.debug(response.getForwardedUrl());
		log.debug(response.getRedirectedUrl());
	}

	@Test
	public void testNormal_GET() throws Exception {
		request.setMethod("GET");
		request.setRequestURI("/normal.gujjy");
		servlet.service(request, response);
		assertThat(request.getAttribute("aa").toString(), is("1"));
		assertThat(response.getForwardedUrl(), is("normal"));
		log.debug(response.getForwardedUrl());
		log.debug(response.getRedirectedUrl());
	}

	@Test
	public void testNormal_POST() throws Exception {
		request.setMethod("POST");
		request.setRequestURI("/normal.gujjy");
		servlet.service(request, response);
		assertThat(request.getAttribute("aa").toString(), is("1"));
		assertThat(response.getForwardedUrl(), is("normal"));
		log.debug(response.getForwardedUrl());
		log.debug(response.getRedirectedUrl());
	}
	
	@Test
	public void testNormal_PUT() throws Exception {
		request.setMethod("PUT");
		request.setRequestURI("/normal.gujjy");
		servlet.service(request, response);
		assertThat(request.getAttribute("aa").toString(), is("1"));
		assertThat(response.getForwardedUrl(), is("normal"));
		log.debug(response.getForwardedUrl());
		log.debug(response.getRedirectedUrl());
	}
	
	@Test
	public void testNormal_DELETE() throws Exception {
		request.setMethod("DELETE");
		request.setRequestURI("/normal.gujjy");
		servlet.service(request, response);
		assertThat(request.getAttribute("aa").toString(), is("1"));
		assertThat(response.getForwardedUrl(), is("normal"));
		log.debug(response.getForwardedUrl());
		log.debug(response.getRedirectedUrl());
	}
	
	@Test(expected=NullPointerException.class)
	public void testNormal_OPTIONS() throws Exception {
		request.setMethod("OPTIONS");
		request.setRequestURI("/normal.gujjy");
		servlet.service(request, response);
		assertThat(request.getAttribute("aa").toString(), is("1"));
		
	}
	
	@Test(expected=NullPointerException.class)
	public void testNormal_TRACE() throws Exception {
		request.setMethod("TRACE");
		request.setRequestURI("/normal.gujjy");
		servlet.service(request, response);
		assertThat(request.getAttribute("aa").toString(), is("1"));
	}
	
	
	@Controller
	static class ExceptionHandleTestController{
		Logger log = Logger.getLogger(this.getClass());
		int count = 0;
		@RequestMapping("/normal")
		public String normal(Model model){
			count ++;
			model.addAttribute("aa", "1");
			log.debug("called");
			return "normal";
		}
		
		
		@SuppressWarnings("unused")
		@RequestMapping("/expectException")
		public String expectException(Model model){
			count ++;
			model.addAttribute("expect", "1");
			log.debug("called");
			
			if(true){
				throw new ExceptionForAnnotationExceptionHandle();
			}
			
			return "expect";
		}
		
		@RequestMapping("/expectCommonException")
		public String expectCommonException(Model model){
			count ++;
			model.addAttribute("expect", "2");
			log.debug("called");
			throw new ExceptionForXmlCommonExceptionHandle();
			
			//return "expectException";
		}
		
		
		@ExceptionHandler(ExceptionForAnnotationExceptionHandle.class)
		public String handleExceptionForExceptionHandle(ExceptionForAnnotationExceptionHandle efeh){
			return "exceptionPage";
		}
	}
	
	@SuppressWarnings("serial")
	static class ExceptionForAnnotationExceptionHandle extends RuntimeException{
		
	}


	@SuppressWarnings("serial")
	static class ExceptionForXmlCommonExceptionHandle extends RuntimeException{
		
	}
}

