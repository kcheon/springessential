package org.springframework.frameworktest.webmvc.exception.handle;

import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;


import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.mock.web.MockServletConfig;
import org.springframework.mock.web.MockServletContext;
import org.springframework.stereotype.Controller;
//import org.springframework.test.annotation.DirtiesContext;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.HandlerExecutionChain;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.annotation.AnnotationMethodHandlerAdapter;
import org.springframework.web.servlet.mvc.annotation.DefaultAnnotationHandlerMapping;


public class DispatcherServletExceptionHandleTest {
	private Logger log = Logger.getLogger(this.getClass());
	private DispatcherServlet servlet;
	
	private AnnotationMethodHandlerAdapter adapter;
	private DefaultAnnotationHandlerMapping mapping;
	private MockHttpServletRequest request;
	private MockHttpServletResponse response;
	private MockServletConfig sConfig;
	private MockServletContext sContext;
	private AnnotationConfigWebApplicationContext wac;
	private ExceptionHandleTestController controller;
	
	@Before
	public void setUp() throws Exception {
		log.debug("setup");
		
		request = new MockHttpServletRequest();
		response = new MockHttpServletResponse();
		sContext = new MockServletContext();
		sConfig = new MockServletConfig(sContext, "gujjy");
		
		wac = new AnnotationConfigWebApplicationContext();
		wac.register(ConfigurationForTest.class);
		wac.setServletConfig(sConfig);
		
		//wac.scan("org.springframework.frameworktest.webmvc.exception.handle");
		
		servlet = new DispatcherServlet(wac);
		servlet.init(sConfig);
		
		adapter = wac.getBean(AnnotationMethodHandlerAdapter.class);
		mapping = wac.getBean(DefaultAnnotationHandlerMapping.class);
		controller = wac.getBean(ExceptionHandleTestController.class);
	}

	@Test
	public void testDi() {
		assertThat(servlet, notNullValue());
		assertThat(wac.getBean(ExceptionHandleTestController.class), notNullValue());
		assertThat(adapter, notNullValue());
		assertThat(mapping, notNullValue());
		assertThat(mapping.getHandlerMap().size(), greaterThan(0));
	}
	
	@Test
	public void tesFindHanlder() throws Exception {
		request.setRequestURI("/normal.gujjy");
		
		HandlerExecutionChain chain = mapping.getHandler(request);
		assertThat(chain, notNullValue());
		assertThat(chain.getHandler().getClass().getCanonicalName(), is(ExceptionHandleTestController.class.getCanonicalName()));
		ModelAndView mav =  adapter.handle(request, response, chain.getHandler());
		assertThat(mav.getModel().get("aa").toString(), is("1"));
		assertThat(controller.count, greaterThan(0));
	}
	
	@Test
	public void testNormal_HEAD() throws Exception {
		request.setMethod("HEAD");
		request.setRequestURI("/normal.gujjy");
		servlet.service(request, response);
		assertThat(controller.count, greaterThan(0));
		assertThat(request.getAttribute("aa").toString(), is("1"));
	}

	@Test
	public void testNormal_GET() throws Exception {
		request.setMethod("GET");
		request.setRequestURI("/normal.gujjy");
		servlet.service(request, response);
		assertThat(controller.count, greaterThan(0));
		assertThat(request.getAttribute("aa").toString(), is("1"));
		
	}

	@Test
	public void testNormal_POST() throws Exception {
		request.setMethod("POST");
		request.setRequestURI("/normal.gujjy");
		servlet.service(request, response);
		assertThat(controller.count, greaterThan(0));
		assertThat(request.getAttribute("aa").toString(), is("1"));
		
	}
	
	@Test
	public void testNormal_PUT() throws Exception {
		request.setMethod("PUT");
		request.setRequestURI("/normal.gujjy");
		servlet.service(request, response);
		assertThat(request.getAttribute("aa").toString(), is("1"));
		
	}
	
	@Test
	public void testNormal_DELETE() throws Exception {
		request.setMethod("DELETE");
		request.setRequestURI("/normal.gujjy");
		servlet.service(request, response);
		assertThat(request.getAttribute("aa").toString(), is("1"));
		
	}
	
	@Test(expected=NullPointerException.class)
	public void testNormal_OPTIONS() throws Exception {
		request.setMethod("OPTIONS");
		request.setRequestURI("/normal.gujjy");
		servlet.service(request, response);
		assertThat(controller.count, not(greaterThan(0)));
		assertThat(request.getAttribute("aa").toString(), is("1"));
	}
	
	@Test(expected=NullPointerException.class)
	public void testNormal_TRACE() throws Exception {
		request.setMethod("TRACE");
		request.setRequestURI("/normal.gujjy");
		servlet.service(request, response);
		assertThat(controller.count, not(greaterThan(0)));
		assertThat(request.getAttribute("aa").toString(), is("1"));
		
	}
	
	
	
	@Configuration
	@ImportResource(value={"classpath:/spring/webcontextTest-servlet.xml"})
	//@DirtiesContext
	static class ConfigurationForTest{
		@Bean
		public ExceptionHandleTestController exceptionHandleTestController(){
			return new ExceptionHandleTestController();
		}
	}
	
	
	@Controller
	static class ExceptionHandleTestController{
		Logger log = Logger.getLogger(this.getClass());
		int count = 0;
		@RequestMapping("/normal")
		public String normal(Model model){
			count ++;
			model.addAttribute("aa", "1");
			log.debug("called");
			return "normal";
		}
		
		
		@SuppressWarnings("unused")
		@RequestMapping("/expectException")
		public String expectException(Model model){
			count ++;
			model.addAttribute("expect", "1");
			log.debug("called");
			
			if(true){
				throw new ExceptionForAnnotationExceptionHandle();
			}
			
			return "expect";
		}
		
		@RequestMapping("/expectCommonException")
		public String expectCommonException(Model model){
			count ++;
			model.addAttribute("expect", "2");
			log.debug("called");
			throw new ExceptionForXmlCommonExceptionHandle();
			
			//return "expectException";
		}
		
		
		@ExceptionHandler(ExceptionForAnnotationExceptionHandle.class)
		public String handleExceptionForExceptionHandle(ExceptionForAnnotationExceptionHandle efeh){
			return "exceptionPage";
		}
	}
	
	@SuppressWarnings("serial")
	static class ExceptionForAnnotationExceptionHandle extends RuntimeException{
		
	}


	@SuppressWarnings("serial")
	static class ExceptionForXmlCommonExceptionHandle extends RuntimeException{
		
	}
}

