package org.springframework.frameworktest.webmvc.exception.handle;

import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;

import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.context.annotation.ImportResource;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.SimpleMappingExceptionResolver;
import org.springframework.web.servlet.mvc.annotation.AnnotationMethodHandlerAdapter;
import org.springframework.web.servlet.mvc.annotation.AnnotationMethodHandlerExceptionResolver;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(/*loader=AnnotationConfigContextLoader.class,*/ classes={ExceptionHandleWithControllerTest.ExceptionHandleTestController.class})
@DirtiesContext
public class ExceptionHandleWithControllerTest {
	Logger log = Logger.getLogger(this.getClass());
	
	@Autowired AnnotationMethodHandlerAdapter adapter;
	@Autowired ExceptionHandleTestController controller;
	@Autowired AnnotationMethodHandlerExceptionResolver resolver;
	@Autowired List<HandlerExceptionResolver> sresolver;
	
	MockHttpServletRequest req;
	MockHttpServletResponse resp;
	
	@Before
	public void setUp() throws Exception {
		req = new MockHttpServletRequest();
		resp = new MockHttpServletResponse();
	}

	@Test
	public void testDi() {
		assertThat(adapter, notNullValue());
		assertThat(controller, notNullValue());
	}
	
	@Test
	public void testNormal() throws Exception {
		req.setRequestURI("/normal");
		
		ModelAndView mav = adapter.handle(req, resp, controller);
		assertThat(mav.getModel().get("aa").toString(), is("1"));
		assertThat(mav.getViewName(), is("normal"));
	}
	
	@Test(expected=ExceptionForAnnotationExceptionHandle.class)
	public void expectException() throws Exception {
		req.setRequestURI("/expectException");
		
		ModelAndView mav = adapter.handle(req, resp, controller);
		assertThat(mav.getModel().get("expect").toString(), is("1"));
		assertThat(mav.getViewName(), is("exceptionPage"));
	}
	
	
	@Test
	public void expectException_annotation_resolve() throws Exception {
		req.setRequestURI("/expectException");
		
		try {
			ModelAndView mav = adapter.handle(req, resp, controller);
			assertThat(mav.getModel().get("expect").toString(), is("1"));
			assertThat(mav.getViewName(), is("expect"));
		} catch (Exception e) {
			ModelAndView mav =  resolver.resolveException(req, resp, controller, e);
			//assertThat(mav.getModel().get("expect").toString(), is("1"));
			assertThat(mav.getViewName(), is("exceptionPage"));
			e.printStackTrace();
		}
	}
	
	@Test
	public void expectException_common_resolve() throws Exception {
		req.setRequestURI("/expectCommonException");
		
		try {
			ModelAndView mav = adapter.handle(req, resp, controller);
			assertThat(mav.getModel().get("expect").toString(), is("1"));
			assertThat(mav.getViewName(), is("expect"));
		} catch (Exception e) {
			
			ModelAndView mav = null ;
			HandlerExceptionResolver processEr = null;
			
			for(HandlerExceptionResolver er : sresolver){
				mav = er.resolveException(req, resp, controller, e);
				processEr = er;
				if(mav != null)
					break;
			}
			assertThat(mav.getViewName(), is("error/exception"));
			assertThat(processEr.getClass(), is(SimpleMappingExceptionResolver.class.getClass()));
		}
	}

	//@Configuration //this is needed when @controller is omitted
	//@ComponentScan("org.springframework.webmvc.exception.handle")
	@ImportResource(value={"classpath:/spring/webcontextTest-servlet.xml"})
	@Controller //this omission is acceptable because @Configuration extends @Component 
	static class ExceptionHandleTestController{
		Logger log = Logger.getLogger(this.getClass());
		
		@RequestMapping("/normal")
		public String normal(Model model){
			model.addAttribute("aa", "1");
			log.debug("called");
			return "normal";
		}
		
		
		@SuppressWarnings("unused")
		@RequestMapping("/expectException")
		public String expectException(Model model){
			model.addAttribute("expect", "1");
			log.debug("called");
			
			if(true){
				throw new ExceptionForAnnotationExceptionHandle();
			}
			
			return "expect";
		}
		
		@RequestMapping("/expectCommonException")
		public String expectCommonException(Model model){
			model.addAttribute("expect", "2");
			log.debug("called");
			throw new ExceptionForXmlCommonExceptionHandle();
			
			//return "expectException";
		}
		
		
		@ExceptionHandler(ExceptionForAnnotationExceptionHandle.class)
		public String handleExceptionForExceptionHandle(ExceptionForAnnotationExceptionHandle efeh){
			return "exceptionPage";
		}
	}
	
	@SuppressWarnings("serial")
	static class ExceptionForAnnotationExceptionHandle extends RuntimeException{
		
	}


	@SuppressWarnings("serial")
	static class ExceptionForXmlCommonExceptionHandle extends RuntimeException{
		
	}

}


