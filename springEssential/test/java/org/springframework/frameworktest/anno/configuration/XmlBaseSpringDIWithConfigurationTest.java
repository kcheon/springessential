package org.springframework.frameworktest.anno.configuration;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import org.springframework.context.annotation.ImportResource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes={XmlBaseSpringDIWithConfigurationTest.ConfigurationClass.class})
public class XmlBaseSpringDIWithConfigurationTest {
Logger log = Logger.getLogger(this.getClass());
	
	@Autowired TestBean tb;
	@Autowired String testStr;
	@Autowired ApplicationContext context;
	
	@Test
	public void testDi() throws Exception {
		assertThat(tb, notNullValue());
		assertThat(testStr, notNullValue());
		assertThat(testStr, is("test"));
		assertThat(context.getBeanDefinitionCount(), is(8));
		for(String name : context.getBeanDefinitionNames()){
			log.debug(name);
		}
	}
	
	
	@ImportResource(value={"classpath:/org/springframework/frameworktest/anno/configuration/contextTest-simple.xml"})
	static class ConfigurationClass{
		@Bean
		public TestBean testBean(){
			return new TestBean();
		}
	}
	
	static class TestBean{
		
	}
}
