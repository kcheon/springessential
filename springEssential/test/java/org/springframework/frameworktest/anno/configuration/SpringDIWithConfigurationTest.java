package org.springframework.frameworktest.anno.configuration;

import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes={SpringDIWithConfigurationTest.ConfigurationClass.class})
public class SpringDIWithConfigurationTest {

	Logger log = Logger.getLogger(this.getClass());
	
	@Autowired TestBean tb;
	@Autowired ApplicationContext context;
	
	@Test
	public void testDi() throws Exception {
		assertThat(tb, notNullValue());
		assertThat(context.getBeanDefinitionCount(), is(7));
		for(String name : context.getBeanDefinitionNames()){
			log.debug(name);
		}
	}
	
	
	static class ConfigurationClass{
		@Bean
		public TestBean testBean(){
			return new TestBean();
		}
	}
	
	static class TestBean{
		
	}
}
