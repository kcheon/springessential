package org.springframework.frameworktest.anno.configuration;

import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;

import org.apache.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(loader=AnnotationConfigContextLoader.class)
public class SpringDIWithAnnotationConfigContextLoaderTest {

	Logger log = Logger.getLogger(this.getClass());
	
	@Autowired TestBean tb;
	@Autowired ApplicationContext context;
	
	@Test
	public void testDi() throws Exception {
		assertThat(tb, notNullValue());
		assertThat(context.getBeanDefinitionCount(), is(7));
		for(String name : context.getBeanDefinitionNames()){
			log.debug(name);
		}
	}
	
	
	@Configuration// this annotation is needed for AnnotationConfigContextLoader
	static class ConfigurationClass{
		@Bean
		public TestBean testBean(){
			return new TestBean();
		}
	}
	
	static class TestBean{
		
	}
}
