package org.springframework.frameworktest;

import static org.junit.Assert.*;
import static org.hamcrest.Matchers.*;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.MutablePropertyValues;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.annotation.AnnotationMethodHandlerAdapter;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:/spring/webcontextTest-servlet.xml"})
public class ControllerBindingTest {
	
	Logger log = Logger.getLogger(this.getClass());
	
	@Autowired ApplicationContext context;
	@Autowired AnnotationMethodHandlerAdapter adapter;
	//@Autowired DefaultAnnotationHandlerMapping mapping;
	
	MockHttpServletRequest req;
	MockHttpServletResponse resp;
	
	
	@Before
	public void setUp() throws Exception{
		req = new MockHttpServletRequest();
		resp = new MockHttpServletResponse();
		
	}
	
	@Test
	public void testDi() throws Exception {
		assertThat(context, notNullValue());
	}

	
	@Test
	public void test_Edit_Binder() throws Exception {
		req.setMethod("post");
		req.setRequestURI("/edit.ever");
		req.setParameter("aa[0]", "bb");//<input type="text" name="aa[0]" ></input>
		req.setParameter("aa[1]", "bbc");//<input type="text" name="aa[0]" ></input>
		
		ModelAndView mav = adapter.handle(req, resp, new TestController());
		Object mAttr = mav.getModel().get("testVo");
		
		assertThat(mAttr, notNullValue());
		assertThat(mAttr.getClass().getName(), is(TestVo.class.getName()));
		TestVo testVo = (TestVo)mAttr;
		assertThat(testVo.getAa().size(), is(2));
		
	}
	
	@Test
	public void test_Edit_Sub_Binder() throws Exception {
		req.setMethod("post");
		req.setRequestURI("/edit.ever");
		req.setParameter("aa[0]", "bb");//<input type="text" name="aa[0]" ></input>
		req.setParameter("aa[1]", "bbc");//<input type="text" name="aa[0]" ></input>
		req.setParameter("kks[0].kk", "되랏!");
		
		ModelAndView mav = adapter.handle(req, resp, new TestController());
		Object mAttr = mav.getModel().get("testVo");
		
		assertThat(mAttr, notNullValue());
		assertThat(mAttr.getClass().getName(), is(TestVo.class.getName()));
		TestVo testVo = (TestVo)mAttr;
		assertThat(testVo.getAa().size(), is(2));
		assertThat(((List<Test3Vo>)testVo.getKks()).get(0).getKk(), is("되랏!"));
	}
	
	@Test
	public void test_insert_Binder() throws Exception {
		req.setMethod("post");
		req.setRequestURI("/insert.ever");
		req.setParameter("aa[0]", "bb");//<input type="text" name="aa[0]" ></input>
		req.setParameter("aa[1]", "bbc");//<input type="text" name="aa[0]" ></input>
		
		ModelAndView mav = adapter.handle(req, resp, new TestController());
		Object mAttr = mav.getModel().get("insertVo");
		
		assertThat(mAttr, notNullValue());
		assertThat(mAttr.getClass().getName(), is(TestVo.class.getName()));
		TestVo testVo = (TestVo)mAttr;
		assertThat(testVo.getAa().size(), is(2));
		
	}
	
	@Test
	public void test_delete_Binder() throws Exception {
		req.setMethod("post");
		req.setRequestURI("/delete.ever");
		req.setParameter("aa[0]", "bb");//<input type="text" name="aa[0]" ></input>
		req.setParameter("aa[1]", "bbc");//<input type="text" name="aa[0]" ></input>
		
		ModelAndView mav = adapter.handle(req, resp, new TestController());
		Object mAttr = mav.getModel().get("testVo");
		
		assertThat(mAttr, notNullValue());
		assertThat(mAttr.getClass().getName(), is(TestVo.class.getName()));
		TestVo testVo = (TestVo)mAttr;
		assertThat(testVo.getAa().size(), is(2));
		
		Object mAttr2 = mav.getModel().get("test2Vo");
		
		assertThat(mAttr2, notNullValue());
		assertThat(mAttr2.getClass().getName(), is(Test2Vo.class.getName()));
		Test2Vo test2Vo = (Test2Vo)mAttr2;
		assertThat(test2Vo.getAa().size(), is(2));
		
		
	}

	@Test
	public void testWebBinder() throws Exception{
		req.setMethod("post");
		req.setRequestURI("/edit.ever");
		req.setParameter("aa[0]", "bb");//<input type="text" name="aa[0]" ></input>
		req.setParameter("aa[1]", "bc");
		req.setParameter("bb", "cc");
		
		TestVo target = new TestVo();
		WebDataBinder binder = new WebDataBinder(target);
		binder.bind(new MutablePropertyValues(req.getParameterMap()));
		assertThat(binder.getBindingResult().getErrorCount(), is(0));
		assertThat(target.getAa().size(), is(2));
	}
	
	
}
class TestController{
	
	Logger log = Logger.getLogger(this.getClass());
	
	@SuppressWarnings("rawtypes")
	@RequestMapping("/edit.ever")
	public void edit(
			HttpServletRequest request
			, HttpSession session
			, @RequestParam Map paramMap
			,TestVo param, Model model){
		
		
		System.out.println(param.getAa());
		System.out.println("size:" +((TestVo)model.asMap().get("testVo")).getAa().size());
	}
	
	@RequestMapping("/insert.ever")
	public void insert(@ModelAttribute("insertVo")TestVo param, Model model){
		log.debug("/insert.ever");
		log.debug("/insert.ever");
		log.debug("/insert.ever");
		log.debug("/insert.ever");
		
		System.out.println(param.getAa());
		System.out.println("size:" +((TestVo)model.asMap().get("insertVo")).getAa().size());
	}
	
	@RequestMapping("/delete.ever")
	public void delete(TestVo param, Test2Vo test2Vo, Model model){
		log.debug("/delete.ever");
		log.debug("/delete.ever");
		log.debug("/delete.ever");
		log.debug("/delete.ever");
		
		System.out.println(param.getAa());
		System.out.println("size:" +((TestVo)model.asMap().get("testVo")).getAa().size());
		
		System.out.println(param.getAa());
		System.out.println("size:" +((Test2Vo)model.asMap().get("test2Vo")).getAa().size());
	}
	
	
	
}

class TestVo{
	private List<String> aa ;
	private String bb;
	private List<Test3Vo> kks ;
	
	private CommonsMultipartFile file;
	
	public List<String> getAa() {
		return aa;
	}
	public void setAa(List<String> aa) {
		this.aa = aa;
	}
	public String getBb() {
		return bb;
	}
	public void setBb(String bb) {
		this.bb = bb;
	}
	public List<Test3Vo> getKks() {
		return kks;
	}
	public void setKks(List<Test3Vo> kks) {
		this.kks = kks;
	}
	public CommonsMultipartFile getFile() {
		return file;
	}
	public void setFile(CommonsMultipartFile file) {
		this.file = file;
	}
	
	
}

class Test2Vo{
	private List<String> aa ;
	private String bb;
	
	public List<String> getAa() {
		return aa;
	}
	public void setAa(List<String> aa) {
		this.aa = aa;
	}
	public String getBb() {
		return bb;
	}
	public void setBb(String bb) {
		this.bb = bb;
	}
	
	
	
}

